/**
 * Un ejemplo que modela un Circulo usando POO
 *
 * @author Ejercicio: (Milton Jesús Vera Contreras - miltonjesusvc@ufps.edu.co)
 * @author Desarrollador: (Jose Florez - florezjoserodolfo@gmail.com)
 *
 * @version 0.000000000000001 :) --> Math.sin(Math.PI-Double.MIN_VALUE)
 */
public class Circulo {

    //COMPLETE las tres propiedades mínimas de un Circulo
    float origenX, origenY, radio;

    public Circulo() {
    }

    public Circulo(float origenX, float origenY, float radio) {
        //COMPLETE
        this.origenX = origenX;
        this.origenY = origenY;
        this.radio = radio;
    }

    /*complete GET / SET*/
    public float getCentroX() {
        return this.origenX;
    }

    public void setCentroX(float origenX) {
        this.origenX = origenX;
    }

    public float getCentroY() {
        return this.origenY;
    }

    public void setCentroY(float origenY) {
        this.origenY = origenY;
    }

    public float getRadio() {
        return this.radio;
    }

    public void setRadio(float radio) {
        this.radio = radio;
    }


    /**
     *
     * @param x coordenada x del punto
     * @param y coordenada y del punto
     * @return
     */
    public String getPosicionPunto(float x, float y) {
        String ubicacion = "Ubicación Desconocida";

        if (this.puntoEstaDentro(x, y)) {
            ubicacion = "Punto Dentro del Círculo";
        } else if (this.puntoEstaEnBorde(x, y)) {
            ubicacion = "Punto En Borde de Círculo";
        } else {
            ubicacion = "Punto Fuera del Círculo";
        }

        return ubicacion;
    }//fin método getPosiciónPunto

    public boolean puntoEstaDentro(float x, float y) {
        if (this.distanciaPunto(x, y) < this.radio) {
            return true;
        }
        return false;//complete
    }//fin método puntoEstaDentro 

    public boolean puntoEstaFuera(float x, float y) {
        if (this.distanciaPunto(x, y) > this.radio) {
            return true;
        }
        return false;//complete
    }//fin método puntoEstaFuera

    public boolean puntoEstaEnBorde(float x, float y) {
        if (this.distanciaPunto(x, y) == this.radio) {
            return true;
        }
        return false;//complete
    }//fin puntoEstaEnBordeSuperior

    public float distanciaPunto(float x, float y) {
        float n = (float) (Math.pow(this.getCentroX() - x, 2));
        float n1 = (float) (Math.pow(this.getCentroY() - y, 2));
        float d = (float) (Math.sqrt(n + n1));
        return d;
    }

}//fin clase Circulo


