### Punto dentro de un Círculo (¿o Circunferencia?)

Determinar la ubicación de un punto relativa a un círculo es un problema geométrico común y de mucha utilidad. Un círculo se determina fácilmente por las coordenadas de su centro y el radio. Una utilidad de este problema se encuentra en los localizadores digitales para dispositivos con soporte de georeferenciación GPS. Por ejemplo, si un radar tiene un espacio de acción circular, es importante determinar si un punto está dentro, fuera o en el borde del área del radar.

1. Determinar si el Punto está dentro del Circulo
2. Determinar si el Punto está fuera del Circulo
3. Determinar si el Punto está en el borde del Circulo
4. El problema no necesita de la clase Punto, solo de una clase Círculo.

Versión Bluej: `4.2.2`

Versión Java: `11.0.6`

Hecho con ♥ por [Jose Florez](https://joseflorez.co)
