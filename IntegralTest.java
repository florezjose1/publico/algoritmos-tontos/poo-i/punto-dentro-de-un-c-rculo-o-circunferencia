import static org.junit.Assert.assertEquals;
import org.junit.Test;
import org.junit.Test;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.Formatter;
import java.util.Locale;

public class IntegralTest {
    public static void main(String[] arg)
    {
        CirculoTest ci = new CirculoTest();
        int grade = 0;

        try {
            ci.testPuntoDentro();
            System.out.println(formatOutput("testPuntoDentro", "25", null));
            grade += 25;
        } catch (AssertionError e) {
            System.out.println(formatOutput("testPuntoDentro", "25", e));
        } catch (Exception e) {
            System.out.println(formatOutput("testPuntoDentro", "25", new AssertionError(e.getMessage())));
        }

        try {
            ci.testPuntoFuera();
            System.out.println(formatOutput("testPuntoFuera", "25", null));
            grade += 25;
        } catch (AssertionError e) {
            System.out.println(formatOutput("testPuntoFuera", "25", e));
        } catch (Exception e) {
            System.out.println(formatOutput("testPuntoFuera", "25", new AssertionError(e.getMessage())));
        }
        
        try {
            ci.testPuntoEstaEnBorde();
            System.out.println(formatOutput("testPuntoEstaEnBorde", "25", null));
            grade += 25;
        } catch (AssertionError e) {
            System.out.println(formatOutput("testPuntoEstaEnBorde", "25", e));
        } catch (Exception e) {
            System.out.println(formatOutput("testPuntoEstaEnBorde", "25", new AssertionError(e.getMessage())));
        }
        
        try {
            ci.getPosicionPunto();
            System.out.println(formatOutput("testgetPosicionPunto", "25", null));
            grade += 25;
        } catch (AssertionError e) {
            System.out.println(formatOutput("testgetPosicionPunto", "25", e));
        } catch (Exception e) {
            System.out.println(formatOutput("testgetPosicionPunto", "25", new AssertionError(e.getMessage())));
        }
        
        System.out.println("Grade :=>> "+grade);

    }

    private static String formatOutput(String testName, String value, AssertionError e) {
        StringBuilder sb = new StringBuilder();
        Formatter f = new Formatter(sb, Locale.getDefault());
        String grade = (e == null ? value : "0");
        f.format("Comment :=>> %s: %s. %s marks\n", testName, (e == null ? "success" : "failure"), grade);
        if (e != null) {
            f.format("********************\n%s\n", e.getMessage());
        }
        return sb.toString();
    }
    
    @Test
    public void testDummy(){IntegralTest.main(null);}

}
