/**
 * Un ejemplo que modela un Circulo usando POO
 * 
 * @author (Milton Jesús Vera Contreras - miltonjesusvc@ufps.edu.co)
 * @version 0.000000000000001 :) --> Math.sin(Math.PI-Double.MIN_VALUE)
 */
public class EjecutableCirculo {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
       Circulo c = new Circulo();
       FrmCirculo gui = new FrmCirculo(c);
       gui.setVisible(true);
    }

}